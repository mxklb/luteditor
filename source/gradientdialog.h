#ifndef GRADIENTDIALOG_H
#define GRADIENTDIALOG_H

#include <QDialog>
#include <QTableWidgetItem>

#include "lookuptable.h"

namespace Ui {
class GradientDialog;
}

class GradientDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GradientDialog(QWidget *parent = 0);
    ~GradientDialog();

    LookUpTable getLookUpTable();
    void clear();

private:
    Ui::GradientDialog *ui;

private slots:
    void stopsCountChanged(int count);
    void itemChanged(QTableWidgetItem* item);
    bool itemIsValid(QTableWidgetItem* item);
};

#endif // GRADIENTDIALOG_H
