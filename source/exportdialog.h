#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>

namespace Ui { class ExportDialog; }

class ExportDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ExportDialog(QWidget *parent = 0);

    void initializeDialog(QSize size, unsigned int steps, QString path);

    QPair<bool, bool> getExportFileTypes();
    unsigned int getDataPoints();
    QString getImageFilePath();
    QString getDataFilePath();
    bool overwriteTemplate();
    QSize getImageSize();

    void setConfigWriteable(bool writeable);

signals:
    void exportFiles();

private slots:
    void exportFilesClicked();
    void selectFolderClicked();
    void fileTypeChanged(QString type);
    void fileNameChanged(QString name);
    void exportTypesChanged();

private:
    Ui::ExportDialog *ui;
    QSize imageSize;
    QString fileName;
    QString exportPath;
    QStringList imageTypes;
    unsigned int dataPoints;
    QPair<bool, bool> exportFileTypes;

    void updateFileNameToolTip();
    void updateExportFileTypes();
    QString getImageType();
};

#endif // EXPORTDIALOG_H
