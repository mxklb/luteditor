#include "exportdialog.h"
#include "ui_export.h"

#include <QFileDialog>

ExportDialog::ExportDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExportDialog)
{
    ui->setupUi(this);
    ui->pushButtonPath->setIcon(QIcon::fromTheme("folder"));

    imageTypes.append(".jpg");
    imageTypes.append(".png");
    ui->comboBoxFileType->addItems(imageTypes);

    dataPoints = 512;
    imageSize = QSize(512, 200);
    exportPath = QDir::homePath();
    fileName = "untitled";

    initializeDialog(imageSize, dataPoints, exportPath);

    connect(ui->pushButtonExport, SIGNAL(clicked()), this, SLOT(exportFilesClicked()));
    connect(ui->pushButtonPath, SIGNAL(clicked()), this, SLOT(selectFolderClicked()));
    connect(ui->comboBoxFileType, SIGNAL(currentIndexChanged(QString)), this, SLOT(fileTypeChanged(QString)));
    connect(ui->lineEditFileName, SIGNAL(textChanged(QString)), this, SLOT(fileNameChanged(QString)));
    connect(ui->checkBoxData, SIGNAL(clicked()), this, SLOT(exportTypesChanged()));

    this->setModal(true);
}

/*
 * Sets the initial image size and interpolation step count
 */
void ExportDialog::initializeDialog(QSize size, unsigned int steps, QString path)
{
    imageSize = size;
    dataPoints = steps;
    exportPath = path;
    ui->spinBoxWidth->setValue(size.width());
    ui->spinBoxHeight->setValue(size.height());
    ui->spinBoxDataPoints->setValue(steps);
    updateExportFileTypes();
}

/*
 * Updates exportFileTypes depending on image and data file checkboxes
 */
void ExportDialog::updateExportFileTypes()
{
    exportFileTypes = QPair<bool, bool>(true, ui->checkBoxData->isChecked());
    updateFileNameToolTip();
}

/*
 * Updates the file name tooltip
 */
void ExportDialog::updateFileNameToolTip()
{
    ui->lineEditFileName->setToolTip( getImageFilePath() );
}

/*
 * Returns the complete image file name (including absolute path and ending)
 */
QString ExportDialog::getImageFilePath()
{
   return exportPath + "/" + fileName + getImageType();
}

/*
 * Returns the image file type (ending, f.e ".jpg")
 */
QString ExportDialog::getImageType()
{
    return ui->comboBoxFileType->currentText();
}

/*
 * Returns the complete data file name (including absolute path and .dat ending)
 */
QString ExportDialog::getDataFilePath()
{
   return exportPath + "/" + fileName + ".dat";
}

/*
 * Close dialog and emit signal exportFiles
 */
void ExportDialog::exportFilesClicked()
{
    this->close();
    emit exportFiles();
}

/*
 * Opens directory chooser dialog and sets filePath according to user selection
 */
void ExportDialog::selectFolderClicked()
{
    QDir dir = QDir(QFileDialog::getExistingDirectory(this, tr("Set Directory"), exportPath, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
    if( dir.exists() ) {
        exportPath = dir.absolutePath();
        updateFileNameToolTip();
    }
}

/*
 * Updates the file name tooltip.
 */
void ExportDialog::fileTypeChanged(QString type)
{
    Q_UNUSED(type)
    updateFileNameToolTip();
}

/*
 * Sets the fileName and updates the file name tooltip if name is not empty
 */
void ExportDialog::fileNameChanged(QString name)
{
    if( name.isEmpty() )
    {
        ui->lineEditFileName->setText(fileName);
        return;
    }

    fileName = name;
    updateFileNameToolTip();
}

/*
 * Slot that calls updateExportFileTypes()
 */
void ExportDialog::exportTypesChanged()
{
    updateExportFileTypes();
}

/*
 * Set enabled checkBoxOverwrite
 */
void ExportDialog::setConfigWriteable(bool writeable)
{
    ui->checkBoxOverwrite->setEnabled(writeable);
    if( !writeable ) ui->checkBoxOverwrite->setToolTip("Template path not writable or template not existing!");
    else ui->checkBoxOverwrite->setToolTip("Overwrite the selected template");
}

/*
 * Returns the choosen image size spin box values
 */
QSize ExportDialog::getImageSize()
{
    return QSize(ui->spinBoxWidth->value(), ui->spinBoxHeight->value());
}

/*
 * Returns true if overwriteTemplate is possible and selected
 */
bool ExportDialog::overwriteTemplate()
{
    return ui->checkBoxOverwrite->isEnabled() && ui->checkBoxOverwrite->isChecked();
}

/*
 * Returns the choosen interpolation steps spin box values
 */
unsigned int ExportDialog::getDataPoints()
{
    return ui->spinBoxDataPoints->value();
}

/*
 * Returns image and data export checkbox states as boolean pair
 */
QPair<bool, bool> ExportDialog::getExportFileTypes()
{
    updateExportFileTypes();
    return exportFileTypes;
}
