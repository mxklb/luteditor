#include "gradientdialog.h"
#include "ui_gradientdialog.h"

GradientDialog::GradientDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GradientDialog)
{
    ui->setupUi(this);
    this->setModal(true);

    ui->tableWidget->setRowCount(2);
    ui->tableWidget->setColumnCount(2);
    ui->tableWidget->setHorizontalHeaderLabels(QString("Position,Color").split(","));
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

    connect(ui->spinBoxStops, SIGNAL(valueChanged(int)), this, SLOT(stopsCountChanged(int)));
    connect(ui->tableWidget, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(itemChanged(QTableWidgetItem*)));
}

GradientDialog::~GradientDialog()
{
    delete ui;
}

void GradientDialog::clear()
{
    ui->spinBoxStops->setValue(2);
    ui->tableWidget->item(0, 0)->setText("");
    ui->tableWidget->item(0, 1)->setText("");
    ui->tableWidget->item(1, 0)->setText("");
    ui->tableWidget->item(1, 1)->setText("");
    ui->lineEditDescription->setText("");
    ui->lineEditName->setText("");
}

void GradientDialog::stopsCountChanged(int count)
{
    if( count > 1 ) ui->tableWidget->setRowCount(count);
    else ui->spinBoxStops->setValue(2);
}

void GradientDialog::itemChanged(QTableWidgetItem* item)
{
    itemIsValid(item);
}

bool GradientDialog::itemIsValid(QTableWidgetItem* item)
{
    if( item == NULL ) return false;

    bool valid = false;
    if( item->column() == 0 ) {
        bool ok = false;
        double position = item->text().toDouble(&ok);
        valid = ok;
    }
    else {
        QColor color(item->text());
        valid = color.isValid();
    }
    return valid;
}

LookUpTable GradientDialog::getLookUpTable()
{
    LookUpTable lut;

    lut.name = ui->lineEditName->text();
    lut.description = ui->lineEditDescription->text();

    for (int row = 0 ; row < ui->tableWidget->rowCount() ; ++row) {
        if( itemIsValid(ui->tableWidget->item(row, 0)) &&
            itemIsValid(ui->tableWidget->item(row, 1)) )
        {
            double position = ui->tableWidget->item(row, 0)->text().toDouble();
            QColor color(ui->tableWidget->item(row, 1)->text());
            lut.colorStops.insert(position, color);
        }
    }

    lut.changeResolution(1024);
    return lut;
}
