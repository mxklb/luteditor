#include "luteditor.h"
#include "ui_luteditor.h"

#include <QGraphicsSceneMouseEvent>
#include <QFileDialog>
#include <QTextStream>
#include <QTimer>

#include <math.h>
#include <limits>
#include <iostream>

using namespace std;

LutEditor::LutEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LutEditor)
{
    ui->setupUi(this);

    direction = Qt::Horizontal;

    numberOfDecimals = settings.value("floats", QVariant::fromValue(6)).toInt();
    interpolationSteps = settings.value("steps", QVariant::fromValue(512)).toInt();
    pixmapSize.setWidth(settings.value("width", QVariant::fromValue(512)).toInt());
    pixmapSize.setHeight(settings.value("height", QVariant::fromValue(200)).toInt());

    colorChooser = new QColorDialog();
    graphicsScene = new QGraphicsScene();
    ui->graphicsView->setScene(graphicsScene);

    gradientDialog = new GradientDialog(NULL);
    exportDialog = new ExportDialog(NULL);

    lutConfigPath = defaultLutConfigPath();

    if( !resetLookUpTables(lutConfigPath) )
    {
        showMessageBox(QString("Initialization Failure"),
                       QString("No gradient config/json files found.\n") +
                       QString("Choose another gradient config path!"),
                       QMessageBox::Critical);
    }

    graphicsScene->addPixmap(createPixmap(pixmapSize, ui->cbDrawTicks->isChecked()));
    graphicsScene->installEventFilter(this);

    exportDialog->initializeDialog(pixmapSize,
                                   interpolationSteps,
                                   settings.value("exportPath", QVariant::fromValue(QDir::homePath())).toString());

    connect(exportDialog, SIGNAL(exportFiles()), this, SLOT(exportFiles()));
    connect(colorChooser, SIGNAL(currentColorChanged(QColor)), this, SLOT(changeSelectedColor(QColor)));
    connect(gradientDialog, SIGNAL(accepted()), this, SLOT(addNewGradient()) );
    connect(ui->cbDrawTicks, SIGNAL(toggled(bool)), this, SLOT(redrawLookUpTable(bool)));
    connect(ui->pbSave, SIGNAL(clicked()), this, SLOT(exportButtonClicked()));
    connect(ui->pbSetPath, SIGNAL(clicked()), this, SLOT(pathButtonClicked()));
    connect(ui->pbAddGradient, SIGNAL(clicked()), this, SLOT(addButtonClicked()));
    connect(ui->cbChooseLut, SIGNAL(currentIndexChanged(int)), this, SLOT(switchLookUpTable(int)));

    ui->pbSetPath->setIcon(QIcon::fromTheme("folder"));
    ui->pbAddGradient->setIcon(QIcon::fromTheme("list-add"));
    ui->pbSave->setIcon(QIcon::fromTheme("document-save"));
}

/*
 * Destructor
 */
LutEditor::~LutEditor()
{
    delete ui;
}

/*
 * Returns the default lut config path:
 * 1) ~/applicationName
 * 2) if exists takes /etc/applicationName/config/ or
 * 3) finally gets the path from app/user settings
 */
QString LutEditor::defaultLutConfigPath()
{
    // Set default lut config path (~/applicationName)
    QString lutPath = QDir::homePath() + "/" + QCoreApplication::applicationName();
    lutPath = QDir::toNativeSeparators(lutPath);

    // Check if debain default path exists
    QString debianDir("/etc/" + QCoreApplication::applicationName() + "/config/");
    if( QFileInfo(debianDir).exists() ) {
        lutPath = debianDir;
    }

    // Get gradient config path from app/user settings
    QString configLutPath = settings.value("lutPath", QVariant::fromValue(lutPath)).toString();
    if( QFileInfo(configLutPath).exists() ) lutPath = configLutPath;

    return QDir::toNativeSeparators(QDir::cleanPath(QDir(lutPath).absolutePath()));
}

/*
 * Parse all *.json files in the given path as gradient definition
 * and initialize the lookupTables member with all found gradients.
 * Note: Remebers correct new paths in app/user settings!
 */
bool LutEditor::resetLookUpTables(QString path)
{
    QDir lutsPath(path);
    QStringList filter = (QStringList() << "*.json");
    QFileInfoList files = lutsPath.entryInfoList(filter);

    lookUpTables.clear();
    foreach(QFileInfo file, files) lookUpTables.append(LookUpTable(file.absoluteFilePath(), interpolationSteps));

    if( lutConfigPath != lutsPath.absolutePath() )
    {
        lutConfigPath = lutsPath.absolutePath();        
        settings.setValue("lutPath", QVariant::fromValue(lutConfigPath));
        settings.sync();
    }

    QFileInfo jsonPath(lutConfigPath);
    exportDialog->setConfigWriteable(jsonPath.isWritable() && indexIsValid());
    ui->pbAddGradient->setEnabled(jsonPath.isWritable());

    ui->pbSetPath->setToolTip(QString("Templates path: %1").arg(lutsPath.absolutePath()));
    initializeLutComboBox();
    return true;
}

/*
 * Shows a message box with given title, text and icon
 */
void LutEditor::showMessageBox(QString title, QString text, QMessageBox::Icon icon)
{
    QMessageBox msgBox;
    msgBox.setIcon(icon);
    msgBox.setWindowTitle(title);
    msgBox.setText(text);
    msgBox.exec();
}

/*
 * Clears and initializes the lut chooser combo box.
 */
void LutEditor::initializeLutComboBox()
{
    ui->cbChooseLut->clear();

    foreach(LookUpTable lut, lookUpTables)
        ui->cbChooseLut->addItem( lut.name );

    selectedLutIdx = lookUpTables.size()-1;
    ui->cbChooseLut->setCurrentIndex(selectedLutIdx);

    switchLookUpTable(selectedLutIdx);
}

/*
 * Updates the look up table description shown in the UI
 */
void LutEditor::updateDescription(QString text)
{
    ui->cbChooseLut->setToolTip(text);
}

/*
 * Reinitializes the lut and changes the amount of color ticks depending on the lut.
 */
void LutEditor::switchLookUpTable(int index)
{
    if( index < 0 ) return;
    selectedLutIdx = index;
    updateDescription(lookUpTables[index].description);
    redrawLookUpTable(ui->cbDrawTicks->isChecked());
}

/*
 * Opens a dialog to choose the path for lut definition files.
 */
void LutEditor::pathButtonClicked()
{
    QFileDialog dialog(this);

    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly,true);
    dialog.setOption(QFileDialog::DontResolveSymlinks);
    dialog.setViewMode(QFileDialog::Detail);
    dialog.setDirectory(lutConfigPath);

    QString newLutPath = lutConfigPath;

    if( !dialog.exec() ) {
        return;
    }

    newLutPath = QDir::cleanPath(QDir(dialog.selectedFiles()[0]).absolutePath());

    if( newLutPath != lutConfigPath ) {
        resetLookUpTables(newLutPath);
    }
}

/*
 * Used to handle mouse click events for the graphicsSceneLut.
 *
 * Recognizes the clicked color tick and opens a color chooser dialog to select a color and applies it to the lut.
 */
bool LutEditor::eventFilter(QObject *object, QEvent *event)
{
    if (object == graphicsScene && event->type() == QEvent::GraphicsSceneMousePress )
    {
        QGraphicsSceneMouseEvent *mouseEvent = static_cast<QGraphicsSceneMouseEvent *>(event);

        if( mouseEvent->button() == Qt::LeftButton )
        {
            QPointF position = mouseEvent->scenePos();

            double realPosition = position.x() / pixmapSize.width();
            bool isInsideBoundingRect = position.x() >= 0 && position.x() < pixmapSize.width();

            if( direction == Qt::Vertical ) {
                isInsideBoundingRect = position.y() >= 0 && position.y() < pixmapSize.height();
                realPosition = (pixmapSize.height() - position.y()) / pixmapSize.height();
            }

            if( isInsideBoundingRect )
            {
                QMap<double, QColor> colorStops = lookUpTables[selectedLutIdx].colorStops;
                selectedColorIdx = nearestIndex(realPosition, colorStops.keys());

                colorChooser->setCurrentColor((colorStops.begin()+selectedColorIdx).value());

                if( colorChooser->exec() == QDialog::Accepted )
                {
                    changeSelectedColor( colorChooser->currentColor() );
                    return true;
                }
            }
        }
    }

    return false;
}

/*
 * Always resizes the graphicsSceneLUT to the actual size of the ui's graphicsView.
 */
void LutEditor::resizeEvent(QResizeEvent *event)
{
    ui->graphicsView->fitInView(graphicsScene->itemsBoundingRect(), Qt::IgnoreAspectRatio);
    QWidget::resizeEvent(event);
}

/*
 * Replaces the color at selectedIdx with the given color and redraws the lut.
 */
void LutEditor::changeSelectedColor(QColor actColor)
{
    QMap<double, QColor> colorStops = lookUpTables[selectedLutIdx].colorStops;
    colorStops[(colorStops.begin()+selectedColorIdx).key()] = actColor;

    // Fix highest and lowest color together
    if( ui->cbFixEdges->isChecked() )
    {
        if( selectedColorIdx == 0 )
        {
            colorStops[(colorStops.begin()+colorStops.size()-1).key()] = colorStops.begin().value();
        }

        if( selectedColorIdx == colorStops.size()-1 )
        {
            colorStops[colorStops.begin().key()] = (colorStops.begin()+colorStops.size()-1).value();
        }
    }

    lookUpTables[selectedLutIdx].colorStops = colorStops;

    redrawLookUpTable(ui->cbDrawTicks->isChecked());
}

/*
 * Clear and redraws the graphic scene => Recreate its pixmap.
 */
void LutEditor::redrawLookUpTable(bool ticksOn)
{
    // Inefficient: Instead addPixmap there should be updatePixmap
    // Workaround: To avoid mem leak, always remove all items before adding.
    QList<QGraphicsItem*> items = graphicsScene->items();
    for (int i = 0; i < items.size(); i++)
    {
        graphicsScene->removeItem(items[i]);
        delete items[i];
    }
    graphicsScene->addPixmap(createPixmap(pixmapSize, ticksOn));
}

/*
 * Opens the export dialog.
 */
void LutEditor::exportButtonClicked()
{
    exportDialog->show();
}

/*
 * Opens the gradient dialog.
 */
void LutEditor::addButtonClicked()
{
    gradientDialog->show();
}

/*
 * Called when new gradient dialog was accepted.
 */
void LutEditor::addNewGradient()
{
    LookUpTable newLut = gradientDialog->getLookUpTable();
    if( newLut.isValid() ) {
        lookUpTables.append(newLut);
        initializeLutComboBox();
        gradientDialog->clear();
    }
    else {
        QMessageBox::warning(this, "Invalid Gradient Definition", "Unable to add the new gradient!");
    }

    QFileInfo jsonPath(lutConfigPath);
    exportDialog->setConfigWriteable(jsonPath.isWritable());
}

/*
 * Get export options from export dialog and saves files to disk.
 */
void LutEditor::exportFiles()
{
    // 1. Save active lut as image file (*.png, *.jpg)
    if( exportDialog->getExportFileTypes().first )
    {
        QString imageFileName = exportDialog->getImageFilePath();
        QSize exportImageSize = exportDialog->getImageSize();

        settings.setValue("exportPath", QVariant::fromValue(QFileInfo(imageFileName).absoluteDir().absolutePath()));
        settings.setValue("width", QVariant::fromValue(exportImageSize.width()));
        settings.setValue("height", QVariant::fromValue(exportImageSize.height()));

        QMessageBox::StandardButton overWrite = QMessageBox::Yes;
        if( QFileInfo(imageFileName).exists() ) {
            overWrite = QMessageBox::question(this, "File already exists", QString("Do you want to overwrite %1?").arg(imageFileName), QMessageBox::Yes|QMessageBox::No);
        }

        if( overWrite == QMessageBox::Yes ) {
            if( !createPixmap(exportImageSize, false).save(imageFileName) ) {
                cerr << "Save file " << imageFileName.toStdString() << " failed!" << endl;
            }
        }
    }

    if( indexIsValid() )
    {
        // 2. Save/overwrite active json config file
        if( exportDialog->overwriteTemplate() )
        {
            QString jsonFileName = QString("%1/%2.json").arg(lutConfigPath).arg(lookUpTables[selectedLutIdx].name.toLower());
            if( !lookUpTables[selectedLutIdx].saveAsJson(jsonFileName, true, numberOfDecimals) )
            {
                cerr << "Save file " << jsonFileName.toStdString() << " failed!" << endl;
            }
        }

        // 3. Save color gradient as text file (*.dat)
        if( exportDialog->getExportFileTypes().second )
        {
            QString dataFileName = exportDialog->getDataFilePath();

            QMessageBox::StandardButton overWrite = QMessageBox::Yes;
            if( QFileInfo(dataFileName).exists() ) {
                overWrite = QMessageBox::question(this, "File already exists", QString("Do you want to overwrite %1?").arg(dataFileName), QMessageBox::Yes|QMessageBox::No);
            }

            settings.setValue("exportPath", QVariant::fromValue(QFileInfo(dataFileName).absoluteDir().absolutePath()));

            if( overWrite == QMessageBox::Yes ) {
                QFile file(dataFileName);
                if( file.open(QIODevice::WriteOnly | QIODevice::Text) )
                {
                    QTextStream out(&file);
                    interpolationSteps = exportDialog->getDataPoints();
                    settings.setValue("steps", QVariant::fromValue(interpolationSteps));

                    for( int i=0; i<lookUpTables.size(); i++ ) {
                        lookUpTables[i].changeResolution(interpolationSteps);
                    }

                    for( unsigned int i=0; i<interpolationSteps; i++ ) {
                        out << i << ", " << lookUpTables[selectedLutIdx].getColor(i).name() << "\n";
                    }
                    file.close();
                }
            }
        }
    }

    settings.sync();
}

/*
 * Returns true if the selectedLutIdx is a valid lookUpTables index
 */
bool LutEditor::indexIsValid()
{
    return !lookUpTables.isEmpty() && selectedLutIdx > -1 && selectedLutIdx < lookUpTables.size();
}

/*
 * Returns the current look up table as a pixmap.
 */
QPixmap LutEditor::createPixmap(QSize size, bool ticksOn)
{
    if( lookUpTables.isEmpty() ) return QPixmap(size);
    return lookUpTables[selectedLutIdx].toPixmap(size, ticksOn, direction);
}

/*
 * Returns the index of the nearest tick to the given position
 */
int LutEditor::nearestIndex(double position, QList<double> ticks)
{
    int index = -1;

    if( ticks.size() < 2 )
        return index;

    double min = std::numeric_limits<int>::max();

    for(int i=0; i<ticks.size(); i++)
    {
        double distance = fabs(position - ticks.at(i));
        if( distance < min )
        {
            min = distance;
            index = i;
        }
    }

    return index;
}
