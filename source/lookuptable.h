#ifndef LOOKUPTABLE_H
#define LOOKUPTABLE_H

#include <QMap>
#include <QColor>
#include <QVariantMap>
#include <QLinearGradient>

class LookUpTable
{
public:
    QString name;
    QString description;
    QMap<double, QColor> colorStops;

    LookUpTable() { resolution = 1024; }
    LookUpTable(QString jsonFile, double numberOfTicks = 1024);

    void changeResolution(int numberOfTicks);

    QColor getColor(double position);
    QColor getColor(unsigned int tick);

    QPixmap toPixmap(QSize size = QSize(512,200), bool drawTicks = true, Qt::Orientation direction = Qt::Horizontal);
    QLinearGradient toLinearGradient();

    bool saveAsJson(QString jsonFile, bool overwrite = false, quint8 floats = 6);
    void printColorStops();
    bool isValid();

private:
    QImage lookUpTable;
    unsigned int resolution;

    bool parseJson(QString fileName);
    QVariantMap readJson(QString fileName);
};

#endif // LOOKUPTABLE_H
