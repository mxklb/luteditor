#include <QApplication>
#include "luteditor.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("mxklb");
    QCoreApplication::setApplicationName("luteditor");

    LutEditor w;
    w.show();
    w.resize(500, w.height());
    return a.exec();
}
