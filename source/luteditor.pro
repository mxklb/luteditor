QT += core gui

greaterThan(QT_MAJOR_VERSION, 4){
    QT += widgets
}

UI_DIR = uics
MOC_DIR = mocs
OBJECTS_DIR = objs

TARGET = luteditor
TEMPLATE = app

SOURCES += main.cpp\
        luteditor.cpp \
        lookuptable.cpp \
    	exportdialog.cpp \
    	gradientdialog.cpp

HEADERS += luteditor.h \
        lookuptable.h \
    	exportdialog.h \
    	gradientdialog.h

FORMS   += luteditor.ui \
    	export.ui \
    	gradientdialog.ui

unix:!macx {
    target.files += $$OUT_PWD/$$TARGET
    target.path = $$[QT_INSTALL_PREFIX]/bin
    INSTALLS += target
}