#include "lookuptable.h"

#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTextStream>
#include <QPainter>

#include <iostream>

/*
 * Initialize look up table from json file
 */
LookUpTable::LookUpTable(QString jsonFile, double numberOfTicks)
{
    parseJson(jsonFile);
    changeResolution(numberOfTicks);
}

/*
 * Set number of interpolation points along the gradient.
 *
 * This defines the resolution of the look up table => the getColor(..) method.
 */
void LookUpTable::changeResolution(int numberOfTicks)
{
    resolution = numberOfTicks;
    lookUpTable = toPixmap(QSize(resolution, 1), false).toImage();
}

/*
 * Checks if the look up table is valid:
 * - has a name
 * - has edge positions 0.0 && 1.0
 * - and all colors are valid
 */
bool LookUpTable::isValid()
{
    bool isValid = !name.isEmpty() && colorStops.size() >= 2;
    if( isValid && colorStops.contains(0.0) && colorStops.contains(1.0) ) {
        foreach (QColor color, colorStops.values()) {
            isValid = isValid && color.isValid();
        }
    }
    return isValid;
}

/*
 * Returns the interpolated color at the given position.
 *
 * Note: Position must be within range 0..1 (firstKey..lastKey).
 */
QColor LookUpTable::getColor(double position)
{
    if( position < colorStops.firstKey() )
        return colorStops.first();

    if( position > colorStops.lastKey() )
        return colorStops.last();

    int lookUp = (int)(position * (resolution-1) + 0.5);

    return QColor(lookUpTable.pixel(lookUp, 0));
}

/*
 * Returns the interpolated gradient color at given position.
 *
 * Note: Position must be within range 0..resolution (numOfTicks).
 */
QColor LookUpTable::getColor(unsigned int tick)
{
    if( tick >= resolution )
        return QColor(lookUpTable.pixel(resolution-1, 0));
    return QColor(lookUpTable.pixel(tick, 0));
}

/*
 * Prints each colors position and the rgb values to std out.
 */
void LookUpTable::printColorStops()
{
    QMapIterator<double, QColor> i(colorStops);
    while( i.hasNext() )
    {
        i.next();
        std::cout << i.key() << "\t: " << i.value().red() << "\t, " << i.value().green() << "\t, " << i.value().blue() << std::endl;
    }
}

/*
 * Reads definition of a look up table from a json file.
 *
 * The json file must contain "Name" && N color stops [0..1] -> Example file content:
 *
 * {
 *      "Name": "major",
 *      "0.000000": "#ff0000",
 *      "0.412599": "#00ff00",
 *      "0.665160": "#0080ff",
 *      "1.000000": "#ff0000"
 * }
 *
 * Optionally provide a "Description": "with informative text".
 *
 * Note: Color positions must be normalized [0..1] && contain entries for 0 and 1
 */
bool LookUpTable::parseJson(QString fileName)
{
    QVariantMap lutConfig = readJson(fileName);

    if( lutConfig.isEmpty() )
        return false;

    QMapIterator<QString, QVariant> mapIterator(lutConfig);
    QMap<double, QColor> stops;
    QString lutName("Unknown");
    QString lutDescription("");

    while( mapIterator.hasNext() )
    {
        mapIterator.next();

        bool isDouble = false;
        QString key = mapIterator.key();
        double stepValue = key.toDouble(&isDouble);

        if( isDouble ) {
            QColor stepColor;
            stepColor.setNamedColor( mapIterator.value().toString() );
            stops.insert( stepValue, stepColor );
        }
        else if( key == QString("Name") )
            lutName = mapIterator.value().toString();
        else if( key == QString("Description") )
            lutDescription = mapIterator.value().toString();
    }

    if( lutName == "Unknown" || stops.size() < 2 )
        return false;

    name = lutName;
    colorStops = stops;
    description = lutDescription;

    return true;
}

/*
 * Parses a json file and returns its content as QVariantMap.
 */
QVariantMap LookUpTable::readJson(QString fileName)
{
    QFile file;
    QByteArray jsonData;

    file.setFileName(fileName);

    if( file.open(QIODevice::ReadOnly | QIODevice::Text) )
        jsonData = file.readAll();

    file.close();

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(jsonData, &error);
    if( doc.isNull() )
        std::cerr << "(Broken json), error code = " << error.errorString().toStdString() << std::endl;

    if( doc.isEmpty() || !doc.isObject() || doc.object().isEmpty() )
    {
        std::cerr << "Error: Bad json document!" << std::endl;
    }

    return doc.object().toVariantMap();
}

/*
 * Writes definition of look up table to a json file.
 */
bool LookUpTable::saveAsJson(QString jsonFile, bool overwrite, quint8 floats)
{
    QFile file(jsonFile);
    if( file.exists() && !overwrite )
    {
        std::cerr << "File " << jsonFile.toStdString() << " already exists!" << std::endl;
        return false;
    }

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    out << "{\n";
    out << QString("  \"%1\": \"%2\",\n").arg("Name").arg(name);
    out << QString("  \"%1\": \"%2\",\n").arg("Description").arg(description);

    QMap<double, QColor>::const_iterator color = colorStops.constBegin();

    while( color != colorStops.constEnd() )
    {
        if( color + 1 != colorStops.constEnd() )
            out << QString("  \"%1\": \"%2\",\n").arg(QString::number(color.key(),'f', floats)).arg(color.value().name());
        else
            out << QString("  \"%1\": \"%2\"\n").arg(QString::number(color.key(),'f', floats)).arg(color.value().name());
        ++color;
    }
    out << "}\n";

    file.close();
    return true;
}

/*
 * Returns a pixmap of given size containing the look up table's linear gradient.
 *
 * Optional ticks are drawn above the look up table and direction is set horizontal or vertical.
 */
QPixmap LookUpTable::toPixmap(QSize size, bool drawTicks, Qt::Orientation direction)
{
    QPointF start = QPointF(0, 0);
    QPointF stop = QPointF(size.width(), 0);

    if( direction == Qt::Vertical )
    {
        start = QPointF(0, size.height());
        stop = QPointF(0, 0);
    }

    QLinearGradient gradient = toLinearGradient();
    gradient.setStart(start);
    gradient.setFinalStop(stop);

    QBrush brush(gradient);
    QPixmap pixmap = QPixmap(size);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.fillRect(pixmap.rect(), brush);

    if( drawTicks )
    {
        int tickPos = 0;
        int sizeX = size.width();
        int sizeY = size.height();
        painter.setPen(QPen(QColor(0,0,0, 125)));

        for(int i=0; i<gradient.stops().size(); i++)
        {
            if( direction == Qt::Vertical ) {
                tickPos = (int)(gradient.stops().at(i).first*(sizeY-1)) + 1;
                painter.drawLine(0,sizeY-tickPos,sizeX,sizeY-tickPos);
            }
            else {
                tickPos = (int)(sizeX-1-gradient.stops().at(i).first*(sizeX-1)) + 1;
                painter.drawLine(sizeX-tickPos,0,sizeX-tickPos,sizeY);
            }
        }
    }

    return pixmap;
}

/*
 * Returns a QLinearGradient of the look up table.
 */
QLinearGradient LookUpTable::toLinearGradient()
{
    QGradientStops gradientStops;
    QMapIterator<double, QColor> mapIterator(colorStops);

    while( mapIterator.hasNext() )
    {
        mapIterator.next();
        gradientStops.push_back( qMakePair<qreal, QColor>((qreal)mapIterator.key(), mapIterator.value()));
    }

    QLinearGradient gradient;
    if( gradientStops.isEmpty() )
        return gradient;

    gradient.setStops(gradientStops);

    return gradient;
}
