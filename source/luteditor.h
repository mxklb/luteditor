#ifndef LUTEDITOR_H
#define LUTEDITOR_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QColorDialog>
#include <QMessageBox>
#include <QSettings>

#include "lookuptable.h"
#include "exportdialog.h"
#include "gradientdialog.h"

namespace Ui { class LutEditor; }

class LutEditor : public QWidget
{
    Q_OBJECT
    
public:
    explicit LutEditor(QWidget *parent = 0);
    ~LutEditor();

protected:
     bool eventFilter(QObject *object, QEvent *event);
     void resizeEvent(QResizeEvent *event);
    
private:
    Ui::LutEditor *ui;
    QSettings settings;
    QGraphicsScene *graphicsScene;
    QColorDialog *colorChooser;
    QList<LookUpTable> lookUpTables;
    ExportDialog *exportDialog;
    GradientDialog *gradientDialog;

    QSize pixmapSize;
    QString lutConfigPath;
    int numberOfDecimals;
    int selectedLutIdx;
    int selectedColorIdx;
    Qt::Orientation direction;
    unsigned int interpolationSteps;

    bool resetLookUpTables(QString path);
    QString defaultLutConfigPath();
    void initializeLutComboBox();
    void updateDescription(QString text);
    QPixmap createPixmap(QSize size, bool ticksOn);
    void showMessageBox(QString title, QString text, QMessageBox::Icon icon = QMessageBox::NoIcon);
    int nearestIndex(double position, QList<double> ticks);
    bool indexIsValid();

private slots:
    void pathButtonClicked();
    void switchLookUpTable(int index);
    void changeSelectedColor(QColor actColor);
    void redrawLookUpTable(bool ticksOn);
    void exportButtonClicked();
    void addButtonClicked();
    void addNewGradient();
    void exportFiles();
};

#endif // LUTEDITOR_H
