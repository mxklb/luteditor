# Great Gradient Editor

An easy to use linear color gradient editor [![build status](https://gitlab.com/mxklb/luteditor/badges/master/pipeline.svg)](https://gitlab.com/mxklb/luteditor/commits/master)

Download latest development version:
- [luteditor.ubuntu14.04_amd64.deb](https://gitlab.com/mxklb/luteditor/builds/artifacts/master/download?job=deploy_trusty)
- [luteditor.ubuntu16.04_amd64.deb](https://gitlab.com/mxklb/luteditor/builds/artifacts/master/download?job=deploy_xenial)

This software can be used to design linear color gradients. To change gradient colors interactively, click the predefined color stops. Gradients can be exported as .png or .jpg images. Additional an export as "look up table"  is possible (a .dat text data file which contains interpolated colors).

![Alt text](images/screenshot.png)

## Gradient Definition

Each gradient is defined within a .json configuration file, for example:

    {
      "Name": "heat-map",
      "Description": "a default heat map",
      "0.000000": "#0000ff",
      "0.330000": "#b200ff",
      "0.660000": "#fff100",
      "1.000000": "#ff001c"
    }

Color gradients between color stops are interpolated linear. All color stops must be defined in the range between 0 and 1 (check example above). All [supported color names](http://doc.qt.io/qt-5/qcolor.html#setNamedColor) can be used. Per default a precision of 6 decimals is used for the color stop positions. Decimal precision may get changed by setting ``floats`` within the *luteditor* configuration (check platform-specific [QSettings](http://doc.qt.io/qt-5/qsettings.html#platform-specific-notes) note to see how to do this).

Note: To select a custom location for .json configuration files -> use the folder button.

## Build Dependencies
This program uses qt, see: [qt-project.org](http://qt-project.org/)

On Ubuntu or Debian fire up:

    sudo apt-get install qt5-default qt5-qmake

On macOS [brew](https://brew.sh/) this:

    brew update
    brew install qt5
    export PATH=$(brew --prefix)/opt/qt5/bin:$PATH

## Build steps
    cd source
    qmake
    make
