#!/bin/bash
pushd `dirname $0` > /dev/null
scriptPath=`pwd`
popd > /dev/null
cd "$scriptPath"

# Prepare deployment
macdeployqt -dmg $scriptPath/../luteditor.app
