# Building Packages

To build installers/packages follow platform specific instructions ..

## Linux / Debian (.deb package)
To build a debian packages just execute

    debian.sh

Note: The build dependencies ``dh_make`` and ``dpkg-buildpackage`` must be installed!

    sudo apt-get install dh-make dpkg-dev

## Mac OS X (.dmg disk image)
To build a package for macOS run

    macOS.sh

Note: The build dependency ``macdeployqt``must be installed!
