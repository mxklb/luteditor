# Gnome desktop launcher

The *luteditor.desktop* file is a gnome desktop application launcher.

To be recognized by the system create a symlink inside */usr/share/applications*

    sudo ln -s /path/to/luteditor/pkgs/devel/luteditor.desktop luteditor.desktop

# Customize personal launcher

Make sure the path in *luteditor.desktop* to the ``Icon`` and the ``Exec`` is correct

    ...
    Icon=/path/to/luteditor/images/luteditor.png
    Exec=/path/to/luteditor/build-luteditor-Qt_5_0_1_qt5-Debug/luteditor
    ...

# Troubleshooting

If the app is not starting try to start *luteditor.desktop* from the terminal:

    grep '^Exec' luteditor.desktop | tail -1 | sed 's/^Exec=//' | sed 's/%.//' &

If dependent libs are missing install all missing libs and try again..

Note: For paths other then */usr/local/lib* just create a symlink to them inside */usr/local/lib*, example:

    cd /usr/local/lib && sudo ln -s ~/path/to/libxy/libxy.so.6 libxy.so.6

Add */usr/local/lib* path to the system:

    sudo ldconfig /usr/local/lib
